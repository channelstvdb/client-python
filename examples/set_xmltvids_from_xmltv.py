#!/usr/bin/env python
#
#
#  channelsdbtv.py
#
#  Copyright 2014 Jonas <jonas@U36SD-jonas>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

__title__ ="set_xmltvids_from_xmltv";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="dev"
__usage__ ='''
ChannelsDBTV
set_xmltvids_from_xmltv: import xmltvids from a xmltv
Version: %(version)s
Author: %(author)s
Usage:
> %(file)s -g ... -x ... [-e ...]
    -g  --grabber_id :  a valid grabber_id for ChannelsDBTV
    -x  --xmltv :       a xmltv file
    -e  --email :       contributor email "name <name@mail.tdl>" or "name@mail.tdl"
'''%{'file':__file__, 'version':__version__,'author':__author__}

import sys, logging
from xml.etree.cElementTree import ElementTree
import simplejson as json
from getopt import getopt
sys.path.append('..')
import channelsdbtv
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s:  %(message)s')
logger = logging.getLogger(__title__)

def main(xmltvFile,grabber_id,email) :
    et = ElementTree().parse(xmltvFile)
    channels = []
    for xmltvChannel in et.findall('channel') :
        alias = xmltvChannel.find('display-name').text
        channel = channelsdbtv.get_channel(alias=alias)['data']['channel']
        if channel :
            logger.info("Channel_id for %s is %s"%(alias,channel['channel_id']))
            channels.append({
                    'channel_id': channel['channel_id'],
                    'xmltvids': [{
                            'grabber_id': grabber_id,
                            'xmltvid': xmltvChannel.attrib['id']
                        }]
                })
        else :
            logger.warning("Channel_id for %s no found. Please add channel for this alias on %s/../alias/unknow#%s"%(alias,channelsdbtv.API_URL,alias))
    data = channelsdbtv.set_channels(channels,email=email)
    sys.stdout.write(json.dumps(data,indent=4))


if __name__ == "__main__" :
    opts,args = getopt(sys.argv[1:], "hx:g:e:",["help", "xmltv=", "grabber_id=", "email="])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(1)

    email = None
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(1)
        elif opt in ("-x", "--xmltv"):
            xmltvFile = arg
        elif opt in ("-g", "--grabber_id"):
            grabber_id = arg
        elif opt in ("-e", "--email"):
            email = arg

    main(xmltvFile,grabber_id,email)
