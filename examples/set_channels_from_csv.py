#!/usr/bin/env python
#
#
#  channelsdbtv.py
#
#  Copyright 2014 Jonas <jonas@U36SD-jonas>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


__title__ ="set_channels_from_csv";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="dev"
__usage__ ='''
ChannelsDBTV
set_channels_from_csv: import channels from a csv
Usage: %(file)s file-utf8.csv
Version: %(version)s
Author: %(author)s
Usage:
> %(file)s file-utf8.csv [-e ...]
    file-utf8.csv: a csv file. first line must be contain field name. Ex:
                    channel_id,name,country,logo
                    ft1.fr,TF1,fr,
                    france2.fr,France 2,fr,http://upload.wikimedia.org/wikipedia/fr/thumb/e/e8/France_2_logo_antenne_(2008).png/60px-France_2_logo_antenne_(2008).png
                    france3.fr,France 3,fr,
    -e  --email:   contributor email "name <name@mail.tdl>" or "name@mail.tdl"
'''%{'file':__file__, 'version':__version__,'author':__author__}

import csv, sys
from getopt import getopt
import simplejson as json
sys.path.append('..')
import channelsdbtv


def main(csvfilepath, email) :
    channels = []
    with open(csvfilepath, 'rb') as csvfile:
        spamreader = csv.reader(csvfile)
        fieldname = spamreader.next()
        for row in spamreader:
            chan = {}
            for i in range(0,len(row)) :
                if row[i] :
                    chan[fieldname[i]] = unicode(row[i],'utf-8')
            channels.append(chan)
    data = channelsdbtv.set_channels(channels, email=email)
    sys.stdout.write(json.dumps(data,indent=4))


if __name__ == "__main__" :
    opts,args = getopt(sys.argv[1:], "h",["help"])
    if len(args) < 1 :
        sys.stdout.write(__usage__)
        sys.exit(1)
    email = None
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(1)
        elif opt in ("-e", "--email"):
            email = arg

    main(args[0], email)

