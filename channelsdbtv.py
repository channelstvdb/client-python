#!/usr/bin/env python
#
#
#  channelsdbtv.py
#
#  Copyright 2014 Jonas <jonas@U36SD-jonas>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
__title__ ="channelsdbtv";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="dev"
__usage__ =''''''


import simplejson as json
import requests, logging
logging.basicConfig(level=logging.INFO,format='%(levelname)s:  %(message)s')
logger = logging.getLogger(__title__)

API_URL = 'http://channelstvdb.mythtv-fr.org/api/'
#~ API_URL = 'http://localhost/Web/api/mythChannels/index.php/api/'

def get_channel(channel_id = None, alias = None, country=None, grabber_id=None, props=None) :
    """
        Get channel with alias or channel_id.
    """
    if channel_id : params = {'id':channel_id}
    else : params = {'alias':alias}
    if country : params['country'] = country
    if grabber_id : params['grabber_id'] = grabber_id
    if props : params['props'] = props
    r = requests.post(API_URL+'get_channel',data = params)
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

def search_channel(q) :
    """
        Search a channel
    """
    r = requests.post(API_URL+'get_channel',{'q':q})
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

def get_grabbers(country=None) :
    """
        Get grabbers
    """
    params = {}
    if country : params['country'] = country
    r = requests.post(API_URL+'get_grabbers',data = params)
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

def get_alias(channel_id) :
    """
        Get alias
    """
    params = {}
    params['channel_id'] = channel_id
    r = requests.post(API_URL+'get_alias',data = params)
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

def set_channels(channels, email=None) :
    """
        Add or update channels with a list of channels dicts
        [(channel_id, name=None, country=None, logo=None, alias=[]), ...]
    """
    params = {'channels':json.dumps(channels)}
    if email : params['email'] = email
    r = requests.post(API_URL+'set_channels',data = params)
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

def set_grabbers(grabbers, email=None) :
    """
        Add or update grabbers with a list of grabbers dicts
        [{grabber_id, name=None, country=None, web=None}, ...]
    """
    params = {'grabbers':json.dumps(grabbers)}
    if email : params['email'] = email
    r = requests.post(API_URL+'set_grabbers',data = params)
    logger.info('Request: %s'%r.url)
    logger.info('Reponse: %s'%r.text)
    return r.json()

